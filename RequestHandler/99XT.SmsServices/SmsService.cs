﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.IO.Ports;

namespace _99XT.SmsServices
{
    public static class SmsService
    {
        private static int baudRate = 0;
        private static int dataBits = 0;
        private static int readTimeOut = 0;
        private static int writeTimeOut = 0;

        private static SerialPort port = new SerialPort();
        private static SmsClient smsClient = new SmsClient();
        private static ShortMessageCollection shortMessageCollection = new ShortMessageCollection();
        private static List<string> ports = new List<string>();

        private static List<string> Ports
        {
            get
            {
                ports = new List<string>();
                try
                {
                    ports.AddRange(SerialPort.GetPortNames());
                }
                catch (Exception ex)
                {
                    LogStatus(ex.Message);
                }
                return ports;
            }
        }

        private static void Connect(string portName)
        {
            try
            {
                if (ports.Contains(portName))
                {
                    port = smsClient.OpenPort(portName, baudRate, dataBits, readTimeOut, writeTimeOut);

                    if (port != null)
                    {
                        LogStatus("Modem is connected at PORT " + portName);
                    }
                    else
                    {
                        LogStatus("Invalid port settings");
                    }
                }
                else
                {
                    LogStatus("Invalid port settings");
                }
            }
            catch (Exception ex)
            {
                LogStatus(ex.Message);
            }

        }
        private static void Disconnect()
        {
            try
            {
                smsClient.ClosePort(port);
            }
            catch (Exception ex)
            {
                LogStatus(ex.Message);
            }
        }

        private static void SendSms(string phoneNumber, string message)
        {
            try
            {
                if (smsClient.sendMsg(port, phoneNumber, message))
                {
                    LogStatus("Message has sent successfully");
                }
                else
                {
                    LogStatus("Failed to send message");
                }

            }
            catch (Exception ex)
            {
                LogStatus(ex.Message);
            }
        }
        private static ShortMessageCollection ReadSms()
        {
            try
            {
                int smsCount = smsClient.CountSMSmessages(port);
                if (smsCount > 0)
                {
                    string command = "AT+CMGL=\"ALL\"";

                    //command = "AT+CMGL=\"REC UNREAD\"";
                    //command = "AT+CMGL=\"STO SENT\"";
                    //command = "AT+CMGL=\"STO UNSENT\"";

                    shortMessageCollection = smsClient.ReadSMS(port, command);
                }
                else
                {
                    shortMessageCollection.Clear();
                    LogStatus("There is no message in SIM");
                }
            }
            catch (Exception ex)
            {
                LogStatus(ex.Message);
            }

            return shortMessageCollection;
        }
        private static void DeleteSms()
        {
            try
            {
                int countSMS = smsClient.CountSMSmessages(port);
                if (countSMS > 0)
                {
                    // string command = "AT+CMGD=1,4"; //Delete all SMS
                    string command = "AT+CMGD=1,3"; //Delete Read SMS
                    if (smsClient.DeleteMsg(port, command))
                    {
                        LogStatus("Messages has deleted successfuly");
                    }
                    else
                    {
                        LogStatus("Failed to delete messages");
                    }
                }
            }
            catch (Exception ex)
            {
                LogStatus(ex.Message);
            }

        }
        private static int CountSms()
        {
            int countSMS = 0;
            try
            {
                countSMS = smsClient.CountSMSmessages(port);
            }
            catch (Exception ex)
            {
                LogStatus(ex.Message);
            }

            return countSMS;
        }

        private static void LogStatus(string Message)
        {
            // TODO: Log Status
            //StreamWriter streamWriter = null;

            //try
            //{
            //    string logFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";
            //    //string pathName = @"E:\";
            //    string pathName = @"SMSapplicationErrorLog_";

            //    string year = DateTime.Now.Year.ToString();
            //    string month = DateTime.Now.Month.ToString();
            //    string day = DateTime.Now.Day.ToString();

            //    string sErrorTime = day + "-" + month + "-" + year;

            //    streamWriter = new StreamWriter(pathName + sErrorTime + ".txt", true);

            //    streamWriter.WriteLine(logFormat + Message);
            //    streamWriter.Flush();

            //}
            //catch (Exception ex)
            //{
            //    //ErrorLog(ex.ToString());
            //}
            //finally
            //{
            //    if (streamWriter != null)
            //    {
            //        streamWriter.Dispose();
            //        streamWriter.Close();
            //    }
            //}

        }
    }
}