﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Diagnostics;

namespace _99XT.Common
{
    public static class ExceptionUtility
    {
        /// <summary>
        /// Handles the exception.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="policy">The policy.</param>
        /// <returns></returns>
        public static bool HandleException(Exception exception, ExceptionPolicyName policy)
        {
            bool result = true;
            //try
            //{
                result = ExceptionPolicy.HandleException(exception, EnumUtility.StringValueOf(policy));
            //}
            //catch (Exception)
            //{ }
            return result;
        }

        public static TResult ExecuteHandler<TResult>(ExceptionRequest<TResult> request)
        {
            TResult result = default(TResult);

            try
            {
                result = request();
            }
            catch (Exception exception)
            {
                if (HandleException(exception, GetExceptionPolicyName()))
                {
                    throw;
                }
            }

            return result;
        }

        private static ExceptionPolicyName GetExceptionPolicyName()
        {
            ExceptionPolicyName result = ExceptionPolicyName.Undefined;

            result = ExceptionPolicyName.RequestHandler;

            return result;
        }
    }
}
