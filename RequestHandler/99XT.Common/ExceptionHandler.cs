﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace _99XT.Common
{
    public static class ExceptionHandler
    {
        /// <summary>
        /// Handles the exception.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="policy">The policy.</param>
        /// <returns></returns>
        public static bool HandleException(Exception exception, string policy)
        {
            return ExceptionPolicy.HandleException(exception, policy);
        }
    }
}
