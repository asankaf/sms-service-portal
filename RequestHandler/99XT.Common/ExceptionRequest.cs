﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _99XT.Common
{
    #region Delegate: ExceptionRequest

    /// <summary>
    /// Delegate: ExceptionRequest
    /// </summary>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    /// <returns>The result.</returns>
    public delegate TResult ExceptionRequest<TResult>();

    #endregion
}
