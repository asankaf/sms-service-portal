﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _99XT.Common
{
    public enum ExceptionPolicyName
    {
        /// <summary>
        /// Undefined Exception Policy
        /// </summary>
        [DescriptionAttribute("DefaultPolicy")]
        Undefined = 0,

        /// <summary>
        /// Request Handler Exception Policy
        /// </summary>
        [DescriptionAttribute("RequestHandlerPolicy")]
        RequestHandler = 1,
    }
}
