﻿using _99XT.HrisServices;
using _99XT.RequestHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _99XT.HrisRequestHandler
{
    public class GeoLocationRequestManager : RequestManager
    {
        protected override ProcessResponse Process(Request request)
        {
            return base.ProcessRequest(request, (processRequest) =>
            {
                ProcessResponse result = new ProcessResponse() { Status = ProcessResponseStatus.Success };

                string firstName = string.Empty;
                List<Contact> contacts = HrisService.GetContacts(firstName);
                string source = request.Source;

                if ((null != contacts) && (0 < contacts.Count))
                {
                    result.Status = ProcessResponseStatus.Failure;
                    result.Message = "HRIS Service failed to Get Geo Location.";
                }
                else
                {
                    // TODO: Call SmsRequestManager or EmailRequestManager base on the source.
                }

                return result;
            });
        }
    }
}
