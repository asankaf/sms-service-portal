﻿using _99XT.HrisServices;
using _99XT.RequestHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _99XT.HrisRequestHandler
{
    public class LeaveRequestManager : RequestManager
    {
        protected override ProcessResponse Process(Request request)
        {
            return base.ProcessRequest(request, (processRequest) =>
            {
                ProcessResponse result = new ProcessResponse() { Status = ProcessResponseStatus.Success };

                bool leaveRequested = HrisService.RequestLeave();

                if (leaveRequested)
                {
                    result.Status = ProcessResponseStatus.Failure;
                    result.Message = "HRIS Service failed to request Leave.";
                }

                return result;
            });
        }
    }
}
