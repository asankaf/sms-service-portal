﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _99XT.HrisServices
{
    public class Contact
    {
        public string EmployeeNumber { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string MobilePhone { get; set; }
        public string HomePhone { get; set; }
        public string GeoLocation { get; set; }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder(string.Empty);

            result.Append("Employee Number: " + EmployeeNumber + "\r\n");
            result.Append("Full Name: " + FullName + "\r\n");
            result.Append("Email: " + Email + "\r\n");
            result.Append("Mobile Phone: " + MobilePhone + "\r\n");
            result.Append("Home Phone: " + HomePhone + "\r\n");
            result.Append("Geo Location: " + GeoLocation + "\r\n");

            return result.ToString();
        }
    }
}
