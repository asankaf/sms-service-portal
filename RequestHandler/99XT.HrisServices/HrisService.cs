﻿using _99XT.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _99XT.HrisServices
{
    public static class HrisService
    {
        public static bool ChangeTimeLog()
        {
            bool result = false;

            result = ExceptionUtility.ExecuteHandler<bool>(() =>
            {
                // TODO : Implement Change Time Log on HRIS.

                return true;
            });

            return result;
        }

        public static bool RequestLeave()
        {
            bool result = false;

            result = ExceptionUtility.ExecuteHandler<bool>(() =>
            {
                // TODO : Implement Request Leave on HRIS.

                return true;
            });

            return result;
        }

        /// <summary>
        /// Find and returns the Contacts by First Name.
        /// </summary>
        /// <param name="firstName">First Name of the Contact.</param>
        /// <returns>Contacts.</returns>
        public static List<Contact> GetContacts(string firstName)
        {
            List<Contact> result = null;

            result = ExceptionUtility.ExecuteHandler<List<Contact>>(() =>
            {
                List<Contact> contact = new List<Contact>();

                // TODO : Implement Get Contact from HRIS.

                return contact;
            });

            return result;
        }

        /// <summary>
        /// Find and returns the Full Name of the Contact by Media Address.
        /// </summary>
        /// <param name="mediaAddress">Mobile Phone Number or the Email Address.</param>
        /// <returns>The Full Name of the Contact.</returns>
        public static string GetFullName(string mediaAddress)
        {
            string result = string.Empty;

            result = ExceptionUtility.ExecuteHandler<string>(() =>
            {
                string fullName = string.Empty;

                // TODO : Implement Get Full Name from HRIS.

                return fullName;
            });

            return result;
        }
    }
}
