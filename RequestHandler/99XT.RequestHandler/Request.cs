﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _99XT.RequestHandler
{
    public class Request
    {
        public int RequestId { get; set; }
        public DateTime RequestedOn { get; set; }
        public User RequestedBy { get; set; }
        public string Source { get; set; }
        public RequestType Type { get; set; }
        public string Body { get; set; }
    }
}
