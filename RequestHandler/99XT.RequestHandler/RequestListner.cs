﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _99XT.RequestHandler
{
    public static class RequestListner
    {
        private static List<RequestManager> requests = new List<RequestManager>();

        public static List<RequestManager> Requests
        {
            get
            {
                return requests;
            }
        }

        public static void Process()
        {
            if ((null != requests) && (0 < requests.Count))
            {
                RequestManager requestManager = requests[0];
                ProcessResponse processResponse = requestManager.Process(requestManager.Request);
                requests.Remove(requestManager);
                Process();
            }
        }
    }
}
