﻿using System;

namespace _99XT.RequestHandler
{
    public class ProcessResponse
    {
        public ProcessResponseStatus Status { get; set; }

        public string Message { get; set; }
    }
}
