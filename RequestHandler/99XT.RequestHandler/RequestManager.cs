﻿
using System;
using _99XT.Common;

namespace _99XT.RequestHandler
{
    public abstract class RequestManager
    {
        public Request Request { get; set; }

        public RequestManager()
        {
        }

        protected internal abstract ProcessResponse Process(Request request);

        protected ProcessResponse ProcessRequest(Request request, ProcessRequest processRequest)
        {
            ProcessResponse result = new ProcessResponse() { Status = ProcessResponseStatus.Success };

            result = ExceptionUtility.ExecuteHandler <ProcessResponse> (() =>
            {
                ProcessResponse processResponse = new ProcessResponse() { Status = ProcessResponseStatus.Success };

                processResponse = processRequest(request);

                return processResponse;
            });

            return result;
        }
    }
}
