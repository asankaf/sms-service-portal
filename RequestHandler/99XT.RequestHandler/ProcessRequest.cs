﻿
namespace _99XT.RequestHandler
{
    #region Delegate: ProcessRequest

    /// <summary>
    /// Delegate: ProcessRequest
    /// </summary>
    /// <param name="proxy">The proxy.</param>
    /// <returns>The result.</returns>
    public delegate ProcessResponse ProcessRequest(Request request);

    #endregion
}