﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _99XT.RequestHandler
{
    public class RequestType
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string RequestTemplate { get; set; }
    }
}
