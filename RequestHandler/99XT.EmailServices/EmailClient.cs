﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace _99XT.EmailServices
{
    public class EmailClient : TcpClient
    {
        public void ConnectPop(string server, int popPort, string username, string password)
        {
            string message;
            string response;

            Connect(server, popPort); //110
            response = Response();
            if (response.Substring(0, 3) != "+OK")
            {
                throw new EmailException(response);
            }

            message = "USER " + username + "\r\n";
            Write(message);
            response = Response();
            if (response.Substring(0, 3) != "+OK")
            {
                throw new EmailException(response);
            }

            message = "PASS " + password + "\r\n";
            Write(message);
            response = Response();
            if (response.Substring(0, 3) != "+OK")
            {
                throw new EmailException(response);
            }
        }

        public void DisconnectPop()
        {
            string message;
            string response;
            message = "QUIT\r\n";
            Write(message);
            response = Response();
            if (response.Substring(0, 3) != "+OK")
            {
                throw new EmailException(response);
            }
        }

        public ArrayList List()
        {
            string message;
            string response;

            ArrayList retval = new ArrayList();
            message = "LIST\r\n";
            Write(message);
            response = Response();
            if (response.Substring(0, 3) != "+OK")
            {
                throw new EmailException(response);
            }

            while (true)
            {
                response = Response();
                if (response == ".\r\n")
                {
                    return retval;
                }
                else
                {
                    EmailMessage msg = new EmailMessage();
                    char[] seps = { ' ' };
                    string[] values = response.Split(seps);
                    msg.number = Int32.Parse(values[0]);
                    msg.bytes = Int32.Parse(values[1]);
                    msg.retrieved = false;
                    retval.Add(msg);
                    continue;
                }
            }
        }

        public EmailMessage Retrieve(EmailMessage rhs)
        {
            string message;
            string response;

            EmailMessage msg = new EmailMessage();
            msg.bytes = rhs.bytes;
            msg.number = rhs.number;

            message = "RETR " + rhs.number + "\r\n";
            Write(message);
            response = Response();
            if (response.Substring(0, 3) != "+OK")
            {
                throw new EmailException(response);
            }

            msg.retrieved = true;
            while (true)
            {
                response = Response();
                if (response == ".\r\n")
                {
                    break;
                }
                else
                {
                    msg.message += response;
                }
            }

            return msg;
        }

        public void Delete(EmailMessage rhs)
        {
            string message;
            string response;

            message = "DELE " + rhs.number + "\r\n";
            Write(message);
            response = Response();
            if (response.Substring(0, 3) != "+OK")
            {
                throw new EmailException(response);
            }
        }

        private void Write(string message)
        {
            ASCIIEncoding encoding = new ASCIIEncoding();

            byte[] WriteBuffer = new byte[1024];
            WriteBuffer = encoding.GetBytes(message);

            NetworkStream stream = GetStream();
            stream.Write(WriteBuffer, 0, WriteBuffer.Length);

            Debug.WriteLine("WRITE:" + message);
        }

        private string Response()
        {
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] serverbuff = new Byte[1024];
            NetworkStream stream = GetStream();
            int count = 0;
            while (true)
            {
                byte[] buff = new Byte[2];
                int bytes = stream.Read(buff, 0, 1);
                if (bytes == 1)
                {
                    serverbuff[count] = buff[0];
                    count++;

                    if (buff[0] == '\n')
                    {
                        break;
                    }
                }
                else
                {
                    break;
                };
            };

            string retval = encoding.GetString(serverbuff, 0, count);
            Debug.WriteLine("READ:" + retval);
            return retval;
        }
    }
}
