﻿using _99XT.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace _99XT.EmailServices
{
    public static class EmailService
    {
        private static readonly MailAddress fromAddress = new MailAddress("noreply@99x.lk", "99X Technology");

        public static string Server { get; set; }
        public static int SmtpPort { get; set; }
        public static int PopPort { get; set; }
        public static string Username { get; set; }
        public static string Password { get; set; }
        public static string Domain { get; set; }

        public static List<EmailMessage> GetEmailMessages()
        {
            List<EmailMessage> result = new List<EmailMessage>();

            result = ExceptionUtility.ExecuteHandler<List<EmailMessage>>(() =>
            {
                List<EmailMessage> emailMessagesFull = new List<EmailMessage>();

                EmailClient emailClient = new EmailClient();
                emailClient.ConnectPop(Server, PopPort, Username, Password);
                ArrayList emailMessages = emailClient.List();

                foreach (EmailMessage emailMessage in emailMessages)
                {
                    EmailMessage emailMessageFull = emailClient.Retrieve(emailMessage);

                    emailMessagesFull.Add(emailMessageFull);
                }

                emailClient.DisconnectPop();

                return emailMessagesFull;
            });

            return result;
        }

        public static bool SendEmailMessage(string subject, string body, List<string> to, List<string> cc, List<string> bcc)
        {
            bool result = false;

            result = ExceptionUtility.ExecuteHandler<bool>(() =>
            {
                SmtpClient smtpClient = new SmtpClient(Server, SmtpPort);
                smtpClient.Credentials = new NetworkCredential(Username, Password, Domain);
                smtpClient.DeliveryFormat = SmtpDeliveryFormat.International;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = fromAddress;
                mailMessage.Sender = fromAddress;

                foreach (string toAddress in to)
                {
                    mailMessage.To.Add(toAddress);
                }

                foreach (string ccAddress in cc)
                {
                    mailMessage.CC.Add(ccAddress);
                }

                foreach (string bccAddress in bcc)
                {
                    mailMessage.Bcc.Add(bccAddress);
                }

                mailMessage.Subject = subject;
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = body;

                smtpClient.Send(mailMessage);
                smtpClient.Dispose();
                smtpClient = null;

                return true;
            });

            return result;
        }
    }
}
