﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _99XT.EmailServices
{
    public class EmailMessage
    {
        public long number;
        public long bytes;
        public bool retrieved;
        public string message;
    }
}
