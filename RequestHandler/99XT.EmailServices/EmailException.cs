﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _99XT.EmailServices
{
    public class EmailException : ApplicationException
    {
        public EmailException(string message)
            : base(message)
        {
        }
    }
}
