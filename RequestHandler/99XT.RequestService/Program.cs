﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
namespace _99XT.RequestService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new X99xRequest() 
            };
            ServiceBase.Run(ServicesToRun);

            /*
            //Creates a new scheduler
            ISchedulerFactory schedFact = new StdSchedulerFactory();
            IScheduler sched = schedFact.GetScheduler();

            sched.Start();

            //Creates a new job <SMSSpooler>
            IJobDetail job = JobBuilder.Create<SMSSpooler>().WithIdentity("SMSSpooler", "99x").Build();

            //Creates a new trigger for the job <SMSSpooler>
            //Executes in every 'interval' seconds
            ITrigger trigger = TriggerBuilder.Create().WithIdentity("SMSTrigger", "99x").StartNow()
                .WithSimpleSchedule(x => x.WithIntervalInSeconds(5).RepeatForever()).Build();

            //Schedule the job <SMSSpooler>
            sched.ScheduleJob(job, trigger);*/
        }
    }
}
